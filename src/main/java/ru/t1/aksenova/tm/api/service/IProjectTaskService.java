package ru.t1.aksenova.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String projectId, String taskId);

    void unbindTaskToProject(String userId, String projectId, String taskId);

    void removeTaskToProject(String userId, String projectId);

}
