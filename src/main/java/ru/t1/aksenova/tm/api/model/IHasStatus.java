package ru.t1.aksenova.tm.api.model;

import ru.t1.aksenova.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
