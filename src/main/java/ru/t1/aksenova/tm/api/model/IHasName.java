package ru.t1.aksenova.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
