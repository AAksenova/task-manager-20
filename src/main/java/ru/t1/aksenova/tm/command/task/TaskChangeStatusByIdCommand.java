package ru.t1.aksenova.tm.command.task;

import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-change-status-by-id";

    public static final String DESCRIPTION = "Change task status by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();

        System.out.println("ENTER PROJECT STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();

        final Status status = Status.toStatus(statusValue);
        getTaskService().changeTaskStatusById(userId, id, status);
    }

}
