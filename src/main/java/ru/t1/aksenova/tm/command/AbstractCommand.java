package ru.t1.aksenova.tm.command;

import ru.t1.aksenova.tm.api.model.ICommand;
import ru.t1.aksenova.tm.api.service.IServiceLocator;
import ru.t1.aksenova.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract String getName();

    public abstract void execute();

    public abstract Role[] getRoles();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public String getUserId() {
        return serviceLocator.getAuthService().getUserId();
    }

    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += ": " + description;
        return result;

    }

}
