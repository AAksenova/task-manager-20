package ru.t1.aksenova.tm.command.task;

import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-show-by-project-id";

    public static final String DESCRIPTION = "Display tasks from project by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[SHOW TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();

        final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        renderTasks(tasks);
    }

}
