package ru.t1.aksenova.tm.repository;

import ru.t1.aksenova.tm.api.repository.IUserOwnedRepository;
import ru.t1.aksenova.tm.exception.field.IndexIncorrectException;
import ru.t1.aksenova.tm.model.AbstractUserOwnerModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnerModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        if (userId == null || model == null) return null;
        model.setUserId(userId);
        add(model);
        return model;
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        final List<M> result = new ArrayList<>();
        for (final M model : records) {
            if (userId.equals(model.getUserId())) result.add(model);
        }
        return result;
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        for (final M model : models) {
            records.remove(model);
        }
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        for (final M model : records) {
            if (!id.equals(model.getId())) continue;
            if (!userId.equals(model.getUserId())) continue;
            return model;
        }
        return null;
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        final int length = records.size();
        if (records.isEmpty() || length <= index) throw new IndexIncorrectException();
        final List<M> models = findAll(userId);
        return models.get(index);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        if (id == null || id.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        final M model = findOneById(userId, id);
        if (model == null) return false;
        return true;
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) return null;
        records.remove(model);
        return model;
    }

    @Override
    public M removeById(final String userId, final String id) {
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

}
